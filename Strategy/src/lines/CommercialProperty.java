package lines;

import lines.offers.CPOffert;

import java.util.List;

public class CommercialProperty extends Line {

    public CommercialProperty(List<CPOffert> offers) {
        super(offers);
    }

    @Override
    public String messages() {
        return "Hello, I am Commercial Property";
    }


}
