import lines.CommercialProperty;
import lines.ILine;
import lines.InlandMarine;
import lines.offers.CPOffert;
import lines.offers.IMOffert;

import java.util.List;

public class Start {
    public static void main(String[] args) {
        List<CPOffert> cpOfferts = List.of(
                new CPOffert(),
                new CPOffert(),
                new CPOffert()
        );

        List<IMOffert> imOfferts = List.of(
                new IMOffert(),
                new IMOffert(),
                new IMOffert()
        );


        ILine line = new CommercialProperty(cpOfferts);

        ILine line1 = new InlandMarine(imOfferts);

        System.out.println(line.messages());
        System.out.println(line1.messages());

    }
}
